const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}



// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10


// Q5 Group users based on their Programming language mentioned in their designation.

// Q1 Find all users who are interested in playing video games.
function findUser(users) {
    let userarr = Object.entries(users).map((index) => {
        return index[1]
    }).filter((object) => {
        if (object.hasOwnProperty("interests")) {
            return object.interest.toString().includes("Video Games")
        } else if (object.hasOwnProperty("interest")) {
            return object.interest.toString().includes("Video Games")
        }
    })
    return userarr
}
console.log(findUser(users))

// Q2 Find all users staying in Germany.

function findUserGermany (users) {
    let userarr = Object.entries(users).map((index) => {
        return index[1]
    }).filter((object) => {
        return object.nationality.toString().includes("Germany")
    })
return userarr
}
console.log(findUserGermany(users))

// Q4 Find all users with masters Degree.

function findUserMAsterDegree (users) {
    let userarr = Object.entries(users).map((index) => {
        return index[1]
    }).filter((object) => {
        return object.qualification.toString().includes("Masters")
    })
return userarr
}
console.log(findUserMAsterDegree(users))
